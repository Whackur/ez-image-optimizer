# default configure

image_config = {
    "quality":85,
    "max_dimension":1000,
    "optimize_into_jpg":True,
    "origin_img_dir_path":"img/",
    "optimized_img_dir_path":"img-optimized/"
}